# ErrorAPI #

The ErrorAPI is a centralized API endpoint that handles all error notification.

Refer Installation, Testing, and Api usage, and Settings to the [wiki](https://bitbucket.org/scooblyboo/errorapi/wiki/Home)